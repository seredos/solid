SOLID Examples
==============

This project is intended to exemplify good and bad variants of the solid principles. Furthermore, different analysis tools like pdepend will be used to show the differences.

### Single Responsibility

#### Example 1
[Source Code](./src/SingleResponsibility/Example1)

```bash
php console single-responsibility:example1
```

In this example, the idea is that each class does only one thing. In the bad example, the User class does both data holding and storage. In the good example, these activities are each handled by a separate class.

|         | Bad Example                          | Good Example                          |
|---------|--------------------------------------|---------------------------------------|
| jDepend | ![](./docs/example1-bad-jdepend.svg) | ![](./docs/example1-good-jdepend.svg) |
| pyramid | ![](./docs/example1-bad-pyramid.svg) | ![](./docs/example1-good-pyramid.svg) |

