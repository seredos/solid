<?php

namespace SOLID\SingleResponsibility\Example1\Good;

use SOLID\SingleResponsibility\Example1\ExampleStorage;

class UserStorage
{
    private ExampleStorage $storage;

    public function __construct(ExampleStorage $storage)
    {
        $this->storage = $storage;
    }

    public function store(User $user): void {
        $this->storage->save($user->getEmail(), $user->getFirstName(), $user->getLastName());
    }
}