
single-responsibility-example1-bad-metrics:
	docker-compose run --rm php vendor/bin/pdepend \
		--jdepend-chart=docs/example1-bad-jdepend.svg \
		--overview-pyramid=docs/example1-bad-pyramid.svg \
		src/SingleResponsibility/Example1/Bad/

single-responsibility-example1-good-metrics:
	docker-compose run --rm php vendor/bin/pdepend \
		--jdepend-chart=docs/example1-good-jdepend.svg \
		--overview-pyramid=docs/example1-good-pyramid.svg \
		src/SingleResponsibility/Example1/Good/

single-responsibility-example1: single-responsibility-example1-bad-metrics single-responsibility-example1-good-metrics

build:
	docker-compose run --rm composer install