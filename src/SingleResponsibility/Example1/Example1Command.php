<?php
declare(strict_types=1);

namespace SOLID\SingleResponsibility\Example1;

use SOLID\SingleResponsibility\Example1\Bad\User as BadUser;
use SOLID\SingleResponsibility\Example1\Good\User;
use SOLID\SingleResponsibility\Example1\Good\UserStorage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Example1Command extends Command
{
    protected static $defaultName = 'single-responsibility:example1';

    protected function configure(): void
    {
        $this->setHelp('executes the example1 code');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $storage = new ExampleStorage($output);

        $output->writeln('<info>execute bad single responsibility example:</info>');

        $this->badExample($storage);

        $output->writeln('<info>execute good single responsibility example:</info>');

        $this->goodExample($storage);

        return Command::SUCCESS;
    }

    private function badExample(ExampleStorage $storage) {
        $badUser = new BadUser($storage);

        $badUser->setEmail('bad@user.de')
            ->setFirstName('bad')
            ->setLastName('user');

        $badUser->store();
    }

    private function goodExample(ExampleStorage $storage) {
        $goodUser = new User();
        $goodUser->setEmail('good@user.de')
            ->setFirstName('good')
            ->setLastName('user');

        $userStorage = new UserStorage($storage);
        $userStorage->store($goodUser);
    }
}