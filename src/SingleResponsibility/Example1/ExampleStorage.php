<?php
declare(strict_types=1);

namespace SOLID\SingleResponsibility\Example1;

use Symfony\Component\Console\Output\OutputInterface;

class ExampleStorage
{
    private OutputInterface $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function save(string $email, string $firstName, string $lastName): void
    {
        $this->output->writeln('mock save example:');
        $this->output->writeln($email . ' ' . $firstName . ' ' . $lastName);
    }
}