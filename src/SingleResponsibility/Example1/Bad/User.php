<?php

namespace SOLID\SingleResponsibility\Example1\Bad;

use SOLID\SingleResponsibility\Example1\ExampleStorage;

class User
{
    private string $firstName;
    private string $lastName;
    private string $email;
    private ExampleStorage $storage;

    public function __construct(ExampleStorage $storage)
    {
        $this->storage = $storage;
    }

    public function store() {
        $this->storage->save($this->email, $this->firstName, $this->lastName);
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName(string $firstName): User
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName(string $lastName): User
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }
}